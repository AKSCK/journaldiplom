﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JournalDiplom
{
    /// <summary>
    /// Логика взаимодействия для SpecialAdd.xaml
    /// </summary>
    public partial class SpecialAdd : Window
    {
        public enum EditorState { Apply, Cancel };
        public EditorState CurretState { get; private set; } = EditorState.Cancel;
        public DataSet1.SpecialtiesRow row = null;
        public DataSet1.SpecialtiesRow Row
        {
            get
            {
                return row;
            }

            set
            {
                row = value;
                row.BeginEdit();

                this.DataContext = Row;
            }

        }
        public SpecialAdd()
        {
            InitializeComponent();


        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string validEror = null;
            validEror += Utilities.CellValidate(Row, "Code", CodEditor.Text);
            validEror += Utilities.CellValidate(Row, "Name", NameEditor.Text);
            if (!String.IsNullOrEmpty(validEror))
            {
                MessageBox.Show($"Не удалось внести изменения {validEror}");
                return;
            }
            CurretState = EditorState.Apply;
            this.Close();


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CurretState = EditorState.Cancel;

            this.Close();

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (CurretState == EditorState.Apply) row.EndEdit();
            else row.CancelEdit();

        }
    }
}
