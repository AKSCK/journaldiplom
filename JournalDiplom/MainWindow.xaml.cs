﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JournalDiplom.DataSet1TableAdapters;
using System.Media;
using System.Windows.Media.Animation;
using System.Windows.Controls.Primitives;

namespace JournalDiplom
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    class Journal
    {
        LecturesTableAdapter lecturesTableAdapter = new LecturesTableAdapter();
        DataSet1.LecturesDataTable lectures = new DataSet1.LecturesDataTable();

        DataSet1.GradesDataTable gradesDataTable = new DataSet1.GradesDataTable();
        GradesTableAdapter gradesTableAdapter = new GradesTableAdapter();

        DataSet1.LectureTypeDataTable lectureTypes = new DataSet1.LectureTypeDataTable();
        LectureTypeTableAdapter lectureTypeTableAdapter = new LectureTypeTableAdapter();

        public DataSet1.LecturesDataTable lecturesOrderBy { get; set; } = new DataSet1.LecturesDataTable();
        //вертуальная таблица позволяющая делать проверку кому пренадлежи оценка
        DataSet1.JournalViewDataTable journalViews = new DataSet1.JournalViewDataTable();

        JournalViewTableAdapter journalViewTableAdapter = new JournalViewTableAdapter();
        StudentsTableAdapter studentsTableAdapter = new StudentsTableAdapter();
        DataSet1.StudentsDataTable students = new DataSet1.StudentsDataTable();
        DataSet1.StudentsDataTable studentsOrderBy;

        DataSet1.JournalDataTable journals = new DataSet1.JournalDataTable();
        JournalTableAdapter JournalTableAdapter = new JournalTableAdapter();
        //поле для хранения сгенерированой нами таблицы
        public DataTable JournalDataTable { get; set; } = new DataTable();

        //поле для хранения графического представления таблицы журнала с оценками
        public DataGrid dataGridJournal = new DataGrid();

        int groupIdJ;
        int discipIdJ;

        public Journal(int groupId, int discipId)
        {
            groupIdJ = groupId;
            discipIdJ = discipId;
            JournalTableAdapter.FillByGroupIdAndDisciplineId(journals, groupId, discipId);
            if (journals.Count < 1)
            {
                DataSet1.JournalRow journalRow = journals.NewJournalRow();
                journalRow.DisciplineId = discipId;
                journalRow.GroupId = groupId;
                journalRow.MaxAllHourse = 0;
                journals.Rows.Add(journalRow);

            }
            lectureTypeTableAdapter.Fill(lectureTypes);
            gradesTableAdapter.Fill(gradesDataTable);
            studentsTableAdapter.FillByGroupId(students, groupId);
            lecturesTableAdapter.FillByGroupIdAndDisciplineId(lectures, groupId, discipId);
            journalViewTableAdapter.FillByGroupIdAndDisciplineId(journalViews, groupId, discipId);
            lecturesOrderBy = (DataSet1.LecturesDataTable)lectures.OrderBy(l => l.DateLecture).OrderBy(l => l.NumberLecture).AsDataView().Table;
            studentsOrderBy = (DataSet1.StudentsDataTable)students.OrderBy(s => s.Name).AsDataView().Table;
            JournalDataTableCreator();
        }

        //Метод ля генерации колонок
        public void ColumnCreator(DataGrid dataGrid)
        {
            //генерация колонки Фамилия
            DataGridTextColumn dataColumn = new DataGridTextColumn();
            dataColumn.Header = " Фамилия";
            dataColumn.IsReadOnly = true;
            //Связываем колонку dataGrid с колонкой сгененированной нами таблицей 
            dataColumn.Binding = new Binding("Surname");
            dataColumn.CanUserReorder = false;

            dataGrid.Columns.Add(dataColumn);

            for (int i = 0; i < lecturesOrderBy.Count; i++)
            {

                dataGrid.Columns.Add(CreateNewJournalColumn(i));
            }

            dataGrid.Columns.Add(CreateAddColum());
            
        }
        //метод для генерации колоноки лекции по заданому индексу
        public DataGridTemplateColumn CreateNewJournalColumn(int columnId)
        {
            DataGridTemplateColumn dataGridTemplateColumn = new DataGridTemplateColumn();
            dataGridTemplateColumn.CellTemplate = CellDataTemplate(columnId);
            dataGridTemplateColumn.CellEditingTemplate = CellEditDataTemplate(columnId);
            dataGridTemplateColumn.HeaderTemplate = ColumnDataTemplate(columnId); ;
            dataGridTemplateColumn.Header = lecturesOrderBy[columnId].id;
            dataGridTemplateColumn.CanUserReorder = false;
            dataGridTemplateColumn.CanUserResize = false;
            dataGridTemplateColumn.CanUserSort = false;
            return dataGridTemplateColumn;
        }
        //Создание сложного заголовка колонки
        public DataTemplate ColumnDataTemplate(int columnId)
        {

            Binding bindDateLecture = new Binding()
            {
                Source = lecturesOrderBy[columnId],
                Path = new PropertyPath("DateLecture"),
                StringFormat = "dd",
                Mode = BindingMode.TwoWay
            };
            Binding bindNumberLecture = new Binding()
            {
                Source = lecturesOrderBy[columnId],
                Path = new PropertyPath("DateLecture"),
                StringFormat = "MM",
                Mode = BindingMode.TwoWay
            };

            var DateDayWithDateLecture = new FrameworkElementFactory(typeof(TextBlock));
            DateDayWithDateLecture.SetBinding(TextBlock.TextProperty, bindDateLecture);
            DateDayWithDateLecture.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Center);

            var DateMonthWithNumberLectur = new FrameworkElementFactory(typeof(TextBlock));
            DateMonthWithNumberLectur.SetBinding(TextBlock.TextProperty, bindNumberLecture);
            DateMonthWithNumberLectur.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Center);

            var buttonFactoryField = new FrameworkElementFactory(typeof(Button));
            //Записываем в событие нажатия кнопки метод JournalShablonMouseUp, RoutedEventHandler - делегат
            buttonFactoryField.AddHandler(
                Button.ClickEvent,
                new RoutedEventHandler(this.JournalShablonMouseUp)
            );

            buttonFactoryField.SetValue(Button.DataContextProperty, lecturesOrderBy[columnId].id);
            //Создаём контекстное меню для колонки
            ContextMenu contextMenuColumn = new ContextMenu();
            MenuItem menuItemGroup = new MenuItem();
            menuItemGroup.Header = "Удалить";
            menuItemGroup.DataContext = lecturesOrderBy[columnId].id;
            menuItemGroup.Click += RemoutLectures;
            contextMenuColumn.Items.Add(menuItemGroup);
            buttonFactoryField.SetValue(Button.ContextMenuProperty, contextMenuColumn);

            SolidColorBrush solidColorBrush = new SolidColorBrush();
            solidColorBrush.Opacity = 0;
            buttonFactoryField.SetValue(Button.BackgroundProperty, solidColorBrush);
            buttonFactoryField.SetValue(Button.BorderBrushProperty, solidColorBrush);

            var grid2FactoryField = new FrameworkElementFactory(typeof(Grid));
            grid2FactoryField.AppendChild(DateDayWithDateLecture);
            grid2FactoryField.AppendChild(DateMonthWithNumberLectur);
            buttonFactoryField.AppendChild(grid2FactoryField);


            var row1 = new FrameworkElementFactory(typeof(RowDefinition));
            var row2 = new FrameworkElementFactory(typeof(RowDefinition));
            row1.SetValue(RowDefinition.HeightProperty, new GridLength(1, GridUnitType.Auto));
            row2.SetValue(RowDefinition.HeightProperty, new GridLength(1, GridUnitType.Auto));

            grid2FactoryField.AppendChild(row1);
            grid2FactoryField.AppendChild(row2);

            DateMonthWithNumberLectur.SetValue(Grid.RowProperty, 1);

            DataTemplate gridFieldTemplate = new DataTemplate();
            gridFieldTemplate.VisualTree = buttonFactoryField;
            return gridFieldTemplate;
        }

        //Создание сложного заголовка колонки
        public DataTemplate CellDataTemplate(int columnId)
        {

            var valuIngrades = new FrameworkElementFactory(typeof(TextBlock));
            valuIngrades.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Center);

            valuIngrades.SetBinding(
                TextBlock.TextProperty,
                new Binding(lecturesOrderBy[columnId].id.ToString())
            );

            DataTemplate gridFieldTemplate = new DataTemplate();
            gridFieldTemplate.VisualTree = valuIngrades;
            return gridFieldTemplate;
        }
        public DataTemplate CellEditDataTemplate(int columnId)
        {

            var valuIngrades = new FrameworkElementFactory(typeof(TextBox));
            valuIngrades.SetValue(TextBox.HorizontalAlignmentProperty, HorizontalAlignment.Stretch);
            valuIngrades.SetValue(TextBox.HorizontalContentAlignmentProperty, HorizontalAlignment.Center);
            valuIngrades.SetValue(TextBox.MaxLengthProperty, 4);
            valuIngrades.AddHandler(TextBox.LoadedEvent, new RoutedEventHandler((o, e) => ((TextBox)o).Focus()));
            //MessageBox.Show(TextBox.LoadedEvent.PropertyType.ToString());
            valuIngrades.SetBinding(
                TextBox.TextProperty,
                new Binding
                {
                    Path = new PropertyPath(lecturesOrderBy[columnId].id.ToString()),
                    Mode = BindingMode.TwoWay
                }
            );
            DataTemplate gridFieldTemplate = new DataTemplate();
            gridFieldTemplate.VisualTree = valuIngrades;
            return gridFieldTemplate;
        }
        //Создание колонки "+" для добавления колонок лекций
        public DataGridTextColumn CreateAddColum()
        {
            DataGridTextColumn accountColumn = new DataGridTextColumn();
            accountColumn.Header = "+";

            var buttonFactoryField = new FrameworkElementFactory(typeof(Button));
            buttonFactoryField.AddHandler(
                Button.ClickEvent,
                new RoutedEventHandler(this.JournalShablonMouseUp)
            );

            buttonFactoryField.SetValue(Button.ContentProperty, "+");
            SolidColorBrush solidColorBrush = new SolidColorBrush();
            solidColorBrush.Opacity = 0;
            buttonFactoryField.SetValue(Button.BackgroundProperty, solidColorBrush);
            buttonFactoryField.SetValue(Button.BorderBrushProperty, solidColorBrush);
            buttonFactoryField.SetValue(Button.HeightProperty, 35.0);

            DataTemplate gridFieldTemplate = new DataTemplate();
            gridFieldTemplate.VisualTree = buttonFactoryField;

            accountColumn.HeaderTemplate = gridFieldTemplate;

            var cellStyle = new Style(typeof(TextBlock));
            cellStyle.Setters.Add(new Setter(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Center));

            accountColumn.ElementStyle = cellStyle;
            accountColumn.CanUserReorder = false;
            accountColumn.CanUserResize = false;
            accountColumn.CanUserSort = false;

            return accountColumn;
        }
        //удаление колонки
        void RemoutLectures(object ob, RoutedEventArgs args)
        {
            MenuItem menuItem = (MenuItem)ob;
            int lecturesId = (int)menuItem.DataContext;
            //получаем колонку контекстное меню которой было задействовано 
            DataGridColumn dataGridColumn = dataGridJournal.Columns.Where(c => c.Header.ToString() == lecturesId.ToString()).ToList()[0];

            DataColumn dataColumn = null;
            //Ищем колонку в сгенерированной нами таблице
            foreach (DataColumn j in JournalDataTable.Columns)
            {
                if (j.ColumnName == lecturesId.ToString()) dataColumn = j;
            }
            if (dataColumn != null)
            {
                //удаление колонки в сгенерированой нами таблицы
                JournalDataTable.Columns.Remove(dataColumn);
            }
            //удаление выбранной лекции
            lecturesOrderBy.Where(l => l.id == lecturesId).ToList()[0].Delete();
            //удаление колонки лекции в графическом представлении
            dataGridJournal.Columns.Remove(dataGridColumn);
        }
        //генерация таблицы на основе таблиц студентов и лекций, а так же заполнение данных  
        void JournalDataTableCreator()
        {
            DataColumn dataColumnId = new DataColumn();
            dataColumnId.ColumnName = "Id";
            DataColumn dataColumnSurname = new DataColumn();
            dataColumnSurname.ColumnName = "Surname";
            DataColumn dataColumnName = new DataColumn();
            dataColumnName.ColumnName = "Name";
            DataColumn dataColumnPatronymic = new DataColumn();
            dataColumnPatronymic.ColumnName = "Patronymic";
            DataColumn dataColumnPhoto = new DataColumn();
            dataColumnPhoto.ColumnName = "Photo";
            JournalDataTable.Columns.Add(dataColumnId);
            JournalDataTable.Columns.Add(dataColumnSurname);
            JournalDataTable.Columns.Add(dataColumnName);
            JournalDataTable.Columns.Add(dataColumnPatronymic);
            JournalDataTable.Columns.Add(dataColumnPhoto);
            //проходимся по таблице лекции с помощью LINQ
            lecturesOrderBy.Where(l =>
            {
                DataColumn dataColumn = new DataColumn();
                dataColumn.ColumnName = l.id.ToString();
                JournalDataTable.Columns.Add(dataColumn);
                return false;

            }).ToList();
            //проходимся по всем студентам генерируем строки, после проходимся по лециям вносим  в ечейки оценки подходящие по условию 
            studentsOrderBy.Where(s =>
            {
                DataRow dataRow = JournalDataTable.NewRow();
                dataRow["Id"] = s.id;
                dataRow["Surname"] = s.Surname;
                dataRow["Name"] = s.Name;
                dataRow["Patronymic"] = s.Patronymic;
                dataRow["Photo"] = s.Photo;
                lecturesOrderBy.Where(l =>
                {
                    var studentList = journalViews.Where(j => j.StudentId == s.id && j.Lecturesid == l.id).ToList();

                    if (studentList.Count == 0 || studentList[0].IsNull("StudentGrade"))
                    {
                        dataRow[l.id.ToString()] = "";

                        return false;
                    }
                    string studentGrade = studentList[0].StudentGrade.ToString();
                    dataRow[l.id.ToString()] = studentGrade;
                    return false;
                }).ToList();
                JournalDataTable.Rows.Add(dataRow);
                return false;
            }).ToList();

            JournalDataTable.AcceptChanges();
        }
        //Экспорт оценок  
        public void GradesExsport()
        {
            JournalTableAdapter.Update(journals);

            for (int lec = 0; lec < lecturesOrderBy.Count; lec++)
            {
                // если лекция помечена на удаление удаляем все соответствующие ей оценки
                if (lecturesOrderBy[lec].RowState == DataRowState.Deleted)
                {

                    for (int stud = 0; stud < studentsOrderBy.Count; stud++)
                    {

                        for (int grade = 0; grade < gradesDataTable.Count; grade++)
                        {
                            if (gradesDataTable[grade].RowState == DataRowState.Deleted) continue;
                            if (gradesDataTable[grade].LectureId == lecturesOrderBy[lec].id && gradesDataTable[grade].StudentId == studentsOrderBy[stud].id)
                            {
                                gradesDataTable[grade].Delete();
                            }
                        }
                    }
                }
                else
                {
                    for (int stud = 0; stud < studentsOrderBy.Count; stud++)
                    {
                        var gradValue = JournalDataTable.Rows[stud].ItemArray[lec + 5].ToString();
                        int tesr = 0;
                        if (!int.TryParse(gradValue, out int res)) continue;

                        bool gradeSearch = false;
                        for (int grade = 0; grade < gradesDataTable.Count; grade++)
                        {
                            if (gradesDataTable[grade].RowState == DataRowState.Deleted) continue;
                            if (gradesDataTable[grade].LectureId == lecturesOrderBy[lec].id && gradesDataTable[grade].StudentId == studentsOrderBy[stud].id)
                            {
                                gradesDataTable[grade].Value = res;
                                gradesDataTable[grade].GradeDate = DateTime.Now;

                                gradeSearch = true;
                            }
                        }

                        if (!gradeSearch)
                        {
                            DataSet1.GradesRow gradesRowNew = gradesDataTable.NewGradesRow();
                            gradesRowNew.LectureId = lecturesOrderBy[lec].id;
                            gradesRowNew.StudentId = studentsOrderBy[stud].id;
                            gradesRowNew.Value = res;
                            gradesRowNew.GradeDate = DateTime.Now;

                            gradesDataTable.Rows.Add(gradesRowNew);
                        }
                    }
                }
            }
            gradesTableAdapter.Update(gradesDataTable);
        }
        //метод добавление лекции
        public void AddLectures(DataGrid dataGrid, DataGridColumn dataGridColumn)
        {
            DataSet1.LecturesRow newLecture = lecturesOrderBy.NewLecturesRow();
            var journalsRows = journals.Where(j => j.GroupId == groupIdJ && j.DisciplineId == discipIdJ).ToList();
            if (journalsRows.Count > 0)
            {
                newLecture.JournalId = journalsRows[0].id;
            }
            else
            {
                var journalsRow = journals.NewJournalRow();
                journalsRow.GroupId = groupIdJ;
                journalsRow.DisciplineId = discipIdJ;
                journals.AddJournalRow(journalsRow);
                JournalTableAdapter.Fill(journals);
            }
            LectureEdit lectureEdit = new LectureEdit();
            lectureEdit.Row = newLecture;
            lectureEdit.LectureTypeDataTable = lectureTypes;
            lectureEdit.ShowDialog();
            if (lectureEdit.CurretState == LectureEdit.EditorState.Apply)
            {
                lecturesOrderBy.Rows.Add(newLecture);

                DataColumn dataColumn = new DataColumn();
                dataColumn.ColumnName = newLecture.id.ToString();
                JournalDataTable.Columns.Add(dataColumn);

                dataGrid.Columns.Remove(dataGrid.Columns[dataGrid.Columns.Count - 1]);

                dataGrid.Columns.Add(CreateNewJournalColumn(lecturesOrderBy.Count - 1));
                dataGrid.Columns.Add(CreateAddColum());
            }
        }
        //метод добавления лекций в базу данных
        public void UpdateLectures()
        {
            lecturesTableAdapter.Update(lecturesOrderBy);
        }
        //метод для обработки нажати на кнопку внутри колонки 
        void JournalShablonMouseUp(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (button.Content.ToString() == "+")
            {
                AddLectures(dataGridJournal, dataGridJournal.Columns[dataGridJournal.Columns.Count - 1]);
            }
            else
            {
                int lecId = (int)button.DataContext;
                DataGridColumn dataGridColumn = dataGridJournal.Columns.Where(c => c.Header.ToString() == lecId.ToString()).ToList()[0];
                int columnId = dataGridJournal.Columns.IndexOf(dataGridColumn);
                DataSet1.LecturesRow newLecture = lecturesOrderBy.Where(l => l.id == lecId).ToList()[0];
                LectureEdit lectureEdit = new LectureEdit();
                lectureEdit.LectureTypeDataTable = lectureTypes;
                lectureEdit.Row = newLecture;
                lectureEdit.ShowDialog();
                if (lectureEdit.CurretState == LectureEdit.EditorState.Apply)
                {
                    dataGridJournal.Columns[columnId].HeaderTemplate = ColumnDataTemplate(columnId - 1);
                }
            }
        }
    }

    public partial class MainWindow : Window
    {
        DataSet1.SpecialtiesDataTable specialtiesDataTable = new DataSet1.SpecialtiesDataTable();
        SpecialtiesTableAdapter specialtiesTableAdapter = new SpecialtiesTableAdapter();
        DataSet1.GroupsDataTable groupsDataTable = new DataSet1.GroupsDataTable();
        GroupsTableAdapter groupsTableAdapter = new GroupsTableAdapter();
        DataSet1.DisciplineDataTable disciplineDataTable = null;
        DisciplineTableAdapter disciplineTableAdapter = new DisciplineTableAdapter();

        StudentsTableAdapter studentsTableAdapter = new StudentsTableAdapter();

        DataSet1.JournalDataTable journals = new DataSet1.JournalDataTable();
        JournalTableAdapter journalTableAdapter = new JournalTableAdapter();

        Journal journal;
        DataGrid dataGridJournal = new DataGrid();
        List<DataGridColumnHeader> dataGridColumnHeaders = new List<DataGridColumnHeader>();
        //строки для генерации пути
        string specName = null;
        string groupName = null;
        string discipName = null;

        int groupId;
        int discpId;

        public MainWindow()
        {
            InitializeComponent();
            journalTableAdapter.Fill(journals);
            specialtiesTableAdapter.Fill(specialtiesDataTable);
            Diacip.SelectionChanged += DiacipSelectionChanged;
            foreach (DataSet1.SpecialtiesRow spRow in specialtiesDataTable)
            {
                TreeViewItem treeViewItem = new TreeViewItem();
                groupsTableAdapter.FillBySpecialityId(groupsDataTable, spRow.id);

                treeViewItem.Header = spRow.Code;
                //сохраняем айди специальности
                treeViewItem.DataContext = spRow.id;

                foreach (DataSet1.GroupsRow groupsRow in groupsDataTable)
                {
                    TreeViewItem treeViewItem2 = new TreeViewItem();
                    treeViewItem2.Header = groupsRow.Name;
                    //сохраняем айди группы
                    treeViewItem2.DataContext = groupsRow.id;
                    treeViewItem2.Selected += ClickGroup;
                    treeViewItem2.ContextMenu = GroupContextMenu(treeViewItem2);
                    treeViewItem.Items.Add(treeViewItem2);
                }
                treeViewItem.ContextMenu = SpecicalContextMenu(treeViewItem);

                Spec.Items.Add(treeViewItem);
            }
            groupsTableAdapter.Fill(groupsDataTable);
        }
        //метод вызываемый при выборе группы
        void ClickGroup(object ob, RoutedEventArgs args)
        {
            TreeViewItem treeViewGroup = (TreeViewItem)ob;
            groupId = (int)treeViewGroup.DataContext;
            disciplineDataTable = new DataSet1.DisciplineDataTable();
            disciplineTableAdapter.FillByGroupId(disciplineDataTable, groupId);
            Diacip.ItemsSource = disciplineDataTable;

            ContextMenu contextMenuDisc = new ContextMenu();
            MenuItem menuItemEdit = new MenuItem();
            menuItemEdit.Header = "Изменить";
            menuItemEdit.Click += EditDiscip;
            contextMenuDisc.Items.Add(menuItemEdit);

            MenuItem menuItemDelet = new MenuItem();
            menuItemDelet.Header = "Удалить";
            menuItemDelet.Click += DeletDiscip;
            contextMenuDisc.Items.Add(menuItemDelet);

            Diacip.ContextMenu = contextMenuDisc;
            groupName = treeViewGroup.Header.ToString();
            specName = ((TreeViewItem)treeViewGroup.Parent).Header.ToString();


        }
        //метод вызываемый при выборе дисциплины служащий для генерации таблицы
        private void DiacipSelectionChanged(object sender, EventArgs e)
        {
            if (Diacip.SelectedIndex < 0) return;
            //записываем в переменую айди выбраной дисциплины
            discpId = disciplineDataTable[Diacip.SelectedIndex].id;
            //если абривиатуры у дисциплины нет то в путь происываем её полное название 
            if (disciplineDataTable[Diacip.SelectedIndex].IsNull("Abbreviation")) discipName = disciplineDataTable[Diacip.SelectedIndex].FullName;
            else discipName = disciplineDataTable[Diacip.SelectedIndex].Abbreviation;
            //создаём обект типа журнал
            journal = new Journal(groupId, discpId);
            // генерируем DataGrid
            DataGrid grid = new DataGrid();
            grid.Background = new SolidColorBrush() { Opacity = 0 };
            grid.CanUserDeleteRows = false;
            grid.CanUserResizeRows = false;
            grid.CanUserAddRows = false;
            grid.MouseUp += Grid_MouseUp;
            grid.AutoGenerateColumns = false;
            Style style = new Style();
            style.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromRgb(205, 205, 205))));
            style.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(0, 0, 1, 1)));
            Style style2 = new Style();
            style2.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromRgb(205, 205, 205))));
            style2.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(1, 0, 1, 0)));
            Style style3 = new Style();
            style3.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromRgb(205, 205, 205))));
            style3.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(0, 0, 0, 1)));


            //заполняем ячейки DataGrid на основе сгенерированой таблицы в journal
            grid.ItemsSource = journal.JournalDataTable.AsDataView();
            grid.HeadersVisibility = DataGridHeadersVisibility.Column;
           // grid.BorderThickness = new Thickness(0, 0, 0, 1);
           // grid.RowDetailsTemplate = RowDetailsDataTemplate();
            //заполняем DataGrid колонками сложного типа
            journal.ColumnCreator(grid);
            dataGridJournal = grid;
            journal.dataGridJournal = grid;
            grid.Margin =new Thickness(5);
            grid.DataContext = groupsDataTable;
            // grid.GridLinesVisibility = DataGridGridLinesVisibility.None;
            grid.HorizontalGridLinesBrush = new SolidColorBrush(Color.FromRgb(205, 205, 205));
            grid.VerticalGridLinesBrush = new SolidColorBrush(Color.FromRgb(205,205,205));
            //удаляем последний элемент грида если там больше 1 элемента это говорит о том, что уже как минимум 1 раз был загружен журнал с оценками    
            if (JournalGrid.Children.Count > 1) JournalGrid.Children.Remove(JournalGrid.Children[JournalGrid.Children.Count - 1]);
            JournalGrid.Children.Add(grid);
            //растягиваем нашу таблицу на 2 колонки 
            Grid.SetColumnSpan(grid, 2);
            //генерируем информацию по нашему расположению и данных журнала (путь, количество часов)
            JournalCreatParametr();
            //grid.RowStyle = style0;
            //grid.ColumnHeaderStyle = style0;
            //grid.CellStyle = style0;
            // grid.Style = style0;
            grid.ColumnHeaderStyle = style;
            grid.BorderBrush = new SolidColorBrush(Color.FromRgb(205, 205, 205));
            grid.SelectionMode = DataGridSelectionMode.Single;
            grid.Columns[0].CanUserSort = false;
            //grid.RowStyle = style2;
            //grid.Style = style3;

        }

        //Выбор студента 
        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var obj_sender = e.OriginalSource as FrameworkElement;
            if (obj_sender == null) return;

            // пробираемся через дебри
            var o1 = VisualTreeHelper.GetParent(obj_sender) as ContentPresenter;
            if (o1 == null) return;
            var o2 = VisualTreeHelper.GetParent(o1) as Border;
            if (o2 == null) return;
            var o3 = VisualTreeHelper.GetParent(o2) as DataGridCell;
            //journal.JournalDataTable.Rows[dataGridJournal.SelectedIndex][0];
            StudentInfoWindow studentInfoWindow = new StudentInfoWindow();
            DataSet1.StudentsDataTable students = new DataSet1.StudentsDataTable();
            if(int.TryParse(journal.JournalDataTable.Rows[dataGridJournal.SelectedIndex][0].ToString(), out int studentId))
            {
                studentsTableAdapter.FillByStudentsId(students, studentId);
                studentInfoWindow.Student = students[0];
                studentInfoWindow.ShowDialog();
            }
            

        }

        public DataTemplate RowDetailsDataTemplate()
        {

            Binding bindSourcePhoto = new Binding()
            {
               
                Path = new PropertyPath("Photo"),
                Mode = BindingMode.TwoWay
            };
            Binding bindSourceSurname = new Binding()
            {

                Path = new PropertyPath("Surname"),
                Mode = BindingMode.TwoWay
            };
            Binding bindSourceName = new Binding()
            {

                Path = new PropertyPath("Name"),
                Mode = BindingMode.TwoWay
            };
            Binding bindSourcePatronymic = new Binding()
            {

                Path = new PropertyPath("Patronymic"),
                Mode = BindingMode.TwoWay
            };
            var PhotoFromStudent = new FrameworkElementFactory(typeof(Image));
            PhotoFromStudent.SetBinding(Image.SourceProperty, bindSourcePhoto);
            PhotoFromStudent.SetValue(Image.HeightProperty, 100.0);
            PhotoFromStudent.SetValue(Image.HorizontalAlignmentProperty, HorizontalAlignment.Left);
            var SurnameFromStudent = new FrameworkElementFactory(typeof(TextBlock));
            SurnameFromStudent.SetBinding(TextBlock.TextProperty, bindSourceSurname);
            var NameFromStudent = new FrameworkElementFactory(typeof(TextBlock));
            NameFromStudent.SetBinding(TextBlock.TextProperty, bindSourceName);
            var PatronymicFromStudent = new FrameworkElementFactory(typeof(TextBlock));
            PatronymicFromStudent.SetBinding(TextBlock.TextProperty, bindSourcePatronymic);

            var PhotoNullFromStudent = new FrameworkElementFactory(typeof(TextBlock));
            PhotoNullFromStudent.SetValue(TextBlock.TextProperty, "\nФото \nНет ");
            PhotoNullFromStudent.SetValue(TextBlock.HeightProperty, 100.0);
            PhotoNullFromStudent.SetValue(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center);
            //Создаём контекстное меню для колонки

            var colum1 = new FrameworkElementFactory(typeof(ColumnDefinition));
            var colum2 = new FrameworkElementFactory(typeof(ColumnDefinition));
            colum1.SetValue(ColumnDefinition.WidthProperty, new GridLength(1, GridUnitType.Auto));
            colum2.SetValue(ColumnDefinition.WidthProperty, new GridLength(1, GridUnitType.Auto));

            var row1 = new FrameworkElementFactory(typeof(RowDefinition));
            var row2 = new FrameworkElementFactory(typeof(RowDefinition));
            var row3 = new FrameworkElementFactory(typeof(RowDefinition));
            row1.SetValue(RowDefinition.HeightProperty, new GridLength(1, GridUnitType.Auto));
            row2.SetValue(RowDefinition.HeightProperty, new GridLength(1, GridUnitType.Auto));
            row3.SetValue(RowDefinition.HeightProperty, new GridLength(1, GridUnitType.Auto));

            SurnameFromStudent.SetValue(Grid.ColumnProperty, 1);
            NameFromStudent.SetValue(Grid.ColumnProperty, 1);
            PatronymicFromStudent.SetValue(Grid.ColumnProperty, 1);

            SurnameFromStudent.SetValue(Grid.RowProperty, 0);
            NameFromStudent.SetValue(Grid.RowProperty, 1);
            PatronymicFromStudent.SetValue(Grid.RowProperty, 2);

            PhotoFromStudent.SetValue(Grid.RowSpanProperty, 3);
            PhotoNullFromStudent.SetValue(Grid.RowSpanProperty, 3);

            var grid2FactoryField = new FrameworkElementFactory(typeof(Grid));
            grid2FactoryField.AppendChild(colum1);
            grid2FactoryField.AppendChild(colum2);
            grid2FactoryField.AppendChild(row1);
            grid2FactoryField.AppendChild(row2);
            grid2FactoryField.AppendChild(row3);

            grid2FactoryField.AppendChild(PhotoNullFromStudent);
            grid2FactoryField.AppendChild(PhotoFromStudent);
            grid2FactoryField.AppendChild(SurnameFromStudent);
            grid2FactoryField.AppendChild(NameFromStudent);
            grid2FactoryField.AppendChild(PatronymicFromStudent);
            DataTemplate gridFieldTemplate = new DataTemplate();
            gridFieldTemplate.VisualTree = grid2FactoryField;
            return gridFieldTemplate;
        }
        //генерируем информацию по нашему расположению и данных журнала (путь, количество часов)
        void JournalCreatParametr()
        {
            var journalSelectet = journals.Where(j => j.GroupId == groupId && j.DisciplineId == discpId).AsDataView()[0];
            PathJournal.Text = $"{specName} / {groupName} / {discipName}";
            EditJournal.DataContext = journalSelectet;
            Grid.SetRowSpan(JournalGrid, 1);
            Grid.SetRow(JournalGrid, 1);
            JournalInfo.Visibility = Visibility.Visible;
        }
        private void EditJournal_Click(object sender, RoutedEventArgs e)
        {
           
            Button button = (Button)sender;
            DataRowView journalRowView = (DataRowView)button.DataContext;
            JournalEditor journalEditor = new JournalEditor();
            journalEditor.RowView = journalRowView;
            journalEditor.ShowDialog();
        }
        void EditGroup(object sender, EventArgs e)
        {
            TreeViewItem groupTree = (TreeViewItem)(((MenuItem)sender).DataContext);
            int groupId = (int)groupTree.DataContext;
            GroupAdd GroupAdd = new GroupAdd();
            DataSet1.GroupsRow groupRow = groupsDataTable.Where(s => s.id == groupId).ToList()[0];
            GroupAdd.Row = groupRow;
            GroupAdd.ShowDialog();
            if (GroupAdd.CurretState == GroupAdd.EditorState.Apply)
            {


                groupsTableAdapter.Update(groupsDataTable);
                //Отображеие новой специальности  

                TreeViewItem treeViewItem = groupTree;
                treeViewItem.Header = groupRow.Name;
                treeViewItem.DataContext = groupRow.id;


                treeViewItem.ContextMenu = GroupContextMenu(treeViewItem);


            }
        }
        void DeletGroup(object sender, EventArgs e)
        {
            TreeViewItem groupTree = (TreeViewItem)(((MenuItem)sender).DataContext);
            int groupId = (int)groupTree.DataContext;
            ((TreeViewItem)groupTree.Parent).Items.Remove(groupTree);
            groupsDataTable.Where(g => g.id == groupId).ToList()[0].Delete();
            groupsTableAdapter.Update(groupsDataTable);
        }
        void AddGroup(object sender, EventArgs e)
        {
            TreeViewItem specTree = (TreeViewItem)(((MenuItem)sender).DataContext);
            int specId = (int)specTree.DataContext;
            GroupAdd groupAdd = new GroupAdd();
            DataSet1.GroupsRow newGroupsRow = groupsDataTable.NewGroupsRow();
            newGroupsRow.SpecialityId = specId;
            groupAdd.Row = newGroupsRow;
            groupAdd.ShowDialog();
            if (groupAdd.CurretState == GroupAdd.EditorState.Apply)
            {
                groupsDataTable.Rows.Add(newGroupsRow);
                groupsTableAdapter.Update(groupsDataTable);
                TreeViewItem treeViewItem2 = new TreeViewItem();
                treeViewItem2.Header = newGroupsRow.Name;
                treeViewItem2.DataContext = newGroupsRow.id;
                treeViewItem2.Selected += ClickGroup;

                treeViewItem2.ContextMenu = GroupContextMenu(treeViewItem2);
                specTree.Items.Add(treeViewItem2);
            }
        }
        void DeletSpec(object sender, EventArgs e)
        {
            TreeViewItem specTree = (TreeViewItem)(((MenuItem)sender).DataContext);
            int specId = (int)specTree.DataContext;
            Spec.Items.Remove(specTree);
            specialtiesDataTable.Where(s => s.id == specId).ToList()[0].Delete();
            specialtiesTableAdapter.Update(specialtiesDataTable);
            // MessageBox.Show("Сначало добавление группы настрой");
        }
        void EditSpec(object sender, EventArgs e)
        {
            TreeViewItem specTree = (TreeViewItem)(((MenuItem)sender).DataContext);
            int specId = (int)specTree.DataContext;
            SpecialAdd specialAdd = new SpecialAdd();
            var specList = specialtiesDataTable.Where(s => s.id == specId).ToList();
            if (specList.Count < 1) return;
            DataSet1.SpecialtiesRow specialtiesRow = specList[0];
            specialAdd.Row = specialtiesRow;
            specialAdd.ShowDialog();
            if (specialAdd.CurretState == SpecialAdd.EditorState.Apply)
            {


                specialtiesTableAdapter.Update(specialtiesDataTable);
                //Отображеие новой специальности  

                TreeViewItem treeViewItem = specTree;
                treeViewItem.Header = specialtiesRow.Code;
                treeViewItem.DataContext = specialtiesRow.id;


                treeViewItem.ContextMenu = SpecicalContextMenu(treeViewItem);


            }
        }

        void StudentWindowStarter(object sender, EventArgs e)
        {
            TreeViewItem groupTree = (TreeViewItem)(((MenuItem)sender).DataContext);
            int groupId = (int)groupTree.DataContext;
            StudWindow studWindow = new StudWindow();
            DataSet1.StudentsDataTable students = new DataSet1.StudentsDataTable();
            studentsTableAdapter.FillByGroupId(students, groupId);
            studWindow.StudentsDataTable = students;
            studWindow.GroupId = groupId;
            studWindow.ShowDialog();
        }


        private void SpecAddClick(object sender, RoutedEventArgs e)
        {
            SpecialAdd specialAdd = new SpecialAdd();
            DataSet1.SpecialtiesRow specialtiesRow = specialtiesDataTable.NewSpecialtiesRow();
            specialAdd.Row = specialtiesRow;
            specialAdd.ShowDialog();
            if (specialAdd.CurretState == SpecialAdd.EditorState.Apply)
            {
                specialtiesDataTable.Rows.Add(specialAdd.Row);
                specialtiesTableAdapter.Update(specialtiesDataTable);
                //Отображеие новой специальности  
                TreeViewItem treeViewItem = new TreeViewItem();
                treeViewItem.Header = specialtiesRow.Code;
                treeViewItem.DataContext = specialtiesRow.id;
                treeViewItem.ContextMenu = SpecicalContextMenu(treeViewItem);
                Spec.Items.Add(treeViewItem);

            }




        }
        //Метод для создания контекстного меню для специальности
        ContextMenu SpecicalContextMenu(TreeViewItem treeViewItem)
        {
            ContextMenu contextMenuSpec = new ContextMenu();
            MenuItem menuItemSpecAddGroup = new MenuItem();
            menuItemSpecAddGroup.Header = "Добавить группу";
            menuItemSpecAddGroup.DataContext = treeViewItem;
            menuItemSpecAddGroup.Click += AddGroup;
            contextMenuSpec.Items.Add(menuItemSpecAddGroup);

            MenuItem menuItemSpecEdit = new MenuItem();
            menuItemSpecEdit.Header = "Изменить";
            menuItemSpecEdit.DataContext = treeViewItem;
            menuItemSpecEdit.Click += EditSpec;
            contextMenuSpec.Items.Add(menuItemSpecEdit);

            MenuItem menuItemSpecDelet = new MenuItem();
            menuItemSpecDelet.Header = "Удалить";
            menuItemSpecDelet.DataContext = treeViewItem;
            menuItemSpecDelet.Click += DeletSpec;
            contextMenuSpec.Items.Add(menuItemSpecDelet);
            return contextMenuSpec;
        }
        //Метод для создания контекстного меню для группы
        ContextMenu GroupContextMenu(TreeViewItem treeViewItem)
        {
            ContextMenu contextMenuGroup = new ContextMenu();
            MenuItem menuItemGroup = new MenuItem();
            menuItemGroup.Header = "Студенты";
            menuItemGroup.DataContext = treeViewItem;
            menuItemGroup.Click += StudentWindowStarter;
            contextMenuGroup.Items.Add(menuItemGroup);

            MenuItem menuItemSpecEdit = new MenuItem();
            menuItemSpecEdit.Header = "Изменить";
            menuItemSpecEdit.DataContext = treeViewItem;
            menuItemSpecEdit.Click += EditGroup;
            contextMenuGroup.Items.Add(menuItemSpecEdit);


            MenuItem menuItemSpecDelet = new MenuItem();
            menuItemSpecDelet.Header = "Удалить";
            menuItemSpecDelet.DataContext = treeViewItem;
            menuItemSpecDelet.Click += DeletGroup;
            contextMenuGroup.Items.Add(menuItemSpecDelet);
            return contextMenuGroup;
        }

        private void AddDiscipButton_Click(object sender, RoutedEventArgs e)
        {
            if (disciplineDataTable == null) return;
            DataSet1.DisciplineRow newDisciplineRow = disciplineDataTable.NewDisciplineRow();
            DiscipplAdd discipplAdd = new DiscipplAdd();
            discipplAdd.Row = newDisciplineRow;
            discipplAdd.ShowDialog();
            if (discipplAdd.CurretState == DiscipplAdd.EditorState.Apply)
            {
                disciplineDataTable.AddDisciplineRow(newDisciplineRow);
                disciplineTableAdapter.Update(disciplineDataTable);

                DataSet1.JournalRow newjournalRow = journals.NewJournalRow();

                newjournalRow.GroupId = groupId;
                newjournalRow.DisciplineId = newDisciplineRow.id;
                journals.AddJournalRow(newjournalRow);
                journalTableAdapter.Update(journals);
            }
        }
        void EditDiscip(object sender, RoutedEventArgs e)
        {

            if (disciplineDataTable == null) return;
            if (Diacip.SelectedIndex < 0) return;
            DataSet1.DisciplineRow newDisciplineRow = disciplineDataTable[Diacip.SelectedIndex];
            DiscipplAdd discipplAdd = new DiscipplAdd();
            discipplAdd.Row = newDisciplineRow;
            discipplAdd.ShowDialog();
            if (discipplAdd.CurretState == DiscipplAdd.EditorState.Apply)
            {
                disciplineTableAdapter.Update(disciplineDataTable);
            }
        }
        void DeletDiscip(object sender, RoutedEventArgs e)
        {
            disciplineDataTable[Diacip.SelectedIndex].Delete();
            disciplineTableAdapter.Update(disciplineDataTable);
        }
        //отменяем изменения
        private void CancelJournal_Click(object sender, RoutedEventArgs e)
        {
            journal?.JournalDataTable.RejectChanges();
        }
        //при нажатии пользователем сохранить, сохраняем лекции и оценки
        private void SaveJournal_Click(object sender, RoutedEventArgs e)
        {
            if (journal == null) return;
            journal.UpdateLectures();
            journal.GradesExsport();
            journalTableAdapter.Update(journals).ToString();
        }
    }
}

