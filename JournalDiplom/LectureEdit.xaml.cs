﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;

namespace JournalDiplom
{
    /// <summary>
    /// Логика взаимодействия для LectureEdit.xaml
    /// </summary>
    public partial class LectureEdit : Window
    {
       
        public enum EditorState { Apply, Cancel };
        public EditorState CurretState { get; private set; } = EditorState.Cancel;
        public DataSet1.LectureTypeDataTable lectureTypeDataTable = null;
        public DataSet1.LectureTypeDataTable LectureTypeDataTable
        {
            get
            {
                return lectureTypeDataTable;
            }
            set
            {
                lectureTypeDataTable = value;
                LecteonTypeCBox.ItemsSource = lectureTypeDataTable;
            }
        }
        public DataSet1.LecturesRow row = null;
        public DataSet1.LecturesRow Row
        {
            get
            {
                return row;
            }

            set
            {
                row = value;
                row.BeginEdit();
                if (!row.IsNull("LecturesTypeId"))
                {
                    LecteonTypeCBox.SelectedIndex = lectureTypeDataTable.Rows.IndexOf(lectureTypeDataTable.Where(l => l.id == row.LecturesTypeId).ToList()[0]);
                }
                else LecteonTypeCBox.SelectedIndex = 0;

                this.DataContext = Row;
            }

        }
        public LectureEdit()
        {
            InitializeComponent();
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string validEror = null;
            validEror += Utilities.CellValidate(Row, "DateLecture", DateLectureDPicker.Text);
            validEror += Utilities.CellValidate(Row, "NumberLecture", NumberLectureTBox.Text);
            validEror += Utilities.CellValidate(Row, "LecturesTopic", LecturesTopicTBox.Text);
            validEror += Utilities.CellValidate(Row, "TheNote", TheNoteTBox.Text);
            if (!String.IsNullOrEmpty(validEror))
            {
                MessageBox.Show($"Не удалось внести изменения {validEror}");
                return;
            }
            DataSet1.LectureTypeRow lectureTypeRow = (DataSet1.LectureTypeRow)((DataRowView)LecteonTypeCBox.SelectedItem).Row;
            row.LecturesTypeId = lectureTypeRow.id;
            CurretState = EditorState.Apply;
            this.Close();


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CurretState = EditorState.Cancel;

            this.Close();

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (CurretState == EditorState.Apply) row.EndEdit();
            else row.CancelEdit();

        }
    }
}
