﻿using JournalDiplom.DataSet1TableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JournalDiplom
{
    /// <summary>
    /// Логика взаимодействия для StudentInfoWindow.xaml
    /// </summary>
    public partial class StudentInfoWindow : Window
    {
        DataSet1.StudentsRow student = null;
        DataSet1.TagsDataTable tags = new DataSet1.TagsDataTable();
        List<DataSet1.TagsRow> newaddtags = new List<DataSet1.TagsRow>();
        DataSet1.TagStudentDataTable tagStudents = new DataSet1.TagStudentDataTable();
        DataSet1.TagStudentDataTable newaddtagStudents = new DataSet1.TagStudentDataTable();
        TagStudentTableAdapter tagStudentTableAdapter = new TagStudentTableAdapter();
        TagsTableAdapter tagsTableAdapter = new TagsTableAdapter();
        List<StackPanel> stackPanels = new List<StackPanel>();
        TextBox TagTBox;
        public DataSet1.StudentsRow Student
        {
            get
            {
                return student;
            }
            set
            {
                student = value;
                Surname.Text = value.Surname;
                Name.Text = value.Name;
                Patronymic.Text = value.Patronymic;
                try
                {
                    SudentPhoto.Source = BitmapFrame.Create(new Uri(value.Photo));
                }
                catch { };
                
                tagsTableAdapter.FillByStudentId(tags, value.id);
                TagWindow.RowDefinitions.Add(new RowDefinition()
                {
                    Height = new GridLength(35)
                });
                StackPanel tagsPanel = new StackPanel();
                tagsPanel.VerticalAlignment = VerticalAlignment.Top;
                tagsPanel.Orientation = Orientation.Horizontal;
                stackPanels.Add(tagsPanel);
                Grid.SetRow(tagsPanel, TagWindow.RowDefinitions.Count - 1);
                TagWindow.Children.Add(tagsPanel);

                foreach (var tag in tags)
                {
                    AddTag(tag);
                }
                TagTBox = new TextBox();
                TagTBox.BorderThickness = new Thickness(0);
                TagTBox.MaxLines = 1;
                TagTBox.MaxLength = 20;
                TagTBox.MinWidth = 50;
               // TagTBox.Margin = new Thickness(0,20,0,5);
                // this.MouseDown += (o, arg) => TadAdd(textBox.Text);
                TagTBox.KeyUp += (o, arg) => 
                {
                    if(arg.Key == Key.Enter) TagAdd(TagTBox.Text);
                };
                this.GotFocus += (o, arg) => TagAdd(TagTBox.Text);
                stackPanels[stackPanels.Count - 1].Children.Add(TagTBox);
                tagStudentTableAdapter.Fill(tagStudents);

            }
        }

        public StudentInfoWindow()
        {
            InitializeComponent();
        }
        //оболочка над добавлением тега служащяя для удаления и добавления поля для ввода текста
        void TagAdd(string tagText)
        {

            if (tagText.Length <= 0 || tagText == null || tagText.Where(t => t != ' ').ToList().Count <= 0) return;
            var children = stackPanels[stackPanels.Count - 1].Children;
            TextBox textBox = (TextBox)children[children.Count-1];
            stackPanels[stackPanels.Count - 1].Children.Remove(textBox);
            AddTag(SuchTagElseCreate(tagText));
            textBox.Text = "";
            stackPanels[stackPanels.Count - 1].Children.Add(textBox);

        }
        DataSet1.TagsRow SuchTagElseCreate(string tagText)
        {
            var tagList = tags.Where(t => 
            {
                if (t.RowState == System.Data.DataRowState.Deleted) return false;
                return t.Name == tagText;
            } ).ToList();
            if(tagList.Count > 0)
            {
               return tagList[0];
                                       
             
                
            }

            DataSet1.TagsDataTable Alltags = new DataSet1.TagsDataTable();
            tagsTableAdapter.Fill(Alltags);
            tagList = Alltags.Where(t => t.Name == tagText).ToList();
            if(tagList.Count > 0)
            {
                var newtagStudents = tagStudents.NewTagStudentRow();
                newtagStudents.StudentId = student.id;
                newtagStudents.TagId = tagList[0].Id;
                tagStudents.AddTagStudentRow(newtagStudents);
                return tagList[0];
            }
            var newTag = tags.NewTagsRow();
            newTag.Name = tagText;
            newTag.Description = "";
            tags.AddTagsRow(newTag);
            var newtagStudents1 = newaddtagStudents.NewTagStudentRow();
            newtagStudents1.StudentId = student.id;
            newtagStudents1.TagsRow = newTag;
            newaddtagStudents.AddTagStudentRow(newtagStudents1);
            newaddtags.Add(newTag);
            return newTag;


        }
        void AddTag(DataSet1.TagsRow tag)
        {
            // получаем список последних тегов


            if(stackPanels.Count <= 0)
            {
                CreatNewStackPanels(NewTagBlock(tag));
                return;
            }
            var lastTags = stackPanels[stackPanels.Count - 1].Children;
            int lengTags = 0;
            for(int i =0; i < lastTags.Count -1; i++)
            {
                lengTags += ((Button)((Grid)lastTags[i]).Children[0]).Content.ToString().Length + 5;
            }
            lengTags += tag.Name.Length;
            
            if (lengTags > 40)
            {
                CreatNewStackPanels(NewTagBlock(tag));
            }
            else
            {
                stackPanels[stackPanels.Count - 1].Children.Add(NewTagBlock(tag));
            }

        }
        //создание кнопки тега
        UIElement NewTagBlock(DataSet1.TagsRow tag)
        {
            Grid grid = new Grid();
            grid.Margin = new Thickness(5);
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition()
            {
                Width = new GridLength(20, GridUnitType.Pixel)
            });
            
            Button tagBtn = new Button();
            tagBtn.BorderThickness = new Thickness(0);
            Border border = new Border();
            //border.Background = new SolidColorBrush();
            border.BorderThickness = new Thickness(1);
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.CornerRadius = new CornerRadius(5);
            border.Margin = new Thickness(-5,-2,0,-5);
            Grid.SetColumnSpan(border, 2);
            tagBtn.Background = new SolidColorBrush()
            {
                Opacity = 0
            };

            tagBtn.Click += (o, e) => MessageBox.Show(tag.Description);
            tagBtn.Content = tag.Name;
            Grid.SetColumn(tagBtn, 0);


            Button tagRemov = new Button();

            tagRemov.Click += (o, e) => {
                ((StackPanel)grid.Parent).Children.Remove(grid);
               
                if (tag.Id == -1)
                {
                    for (int i = 0; i < newaddtags.Count; i++)
                    {
                        if (newaddtags[i] == tag)
                        {
                            newaddtags[i].Delete();
                            newaddtagStudents[i].Delete();
                            break;
                        }
                    }
                }
                else
                {
                    var tagStuden = tagStudents.Where(t => 
                    {
                        if (t.RowState == System.Data.DataRowState.Deleted) return false;
                        if (t.IsNull("id")) return false;
                        return t.TagId == tag.Id;
                       
                    } ).ToList();
                    if (tagStuden.Count > 1)
                    {
                        tagStuden.Where(t => t.StudentId == student.id).ToList()[0].Delete();
                    }
                    else if (tagStuden.Count == 1)
                    {
                        tagStuden.Where(t => t.StudentId == student.id).ToList()[0].Delete();
                        tag.Delete();

                    }
                    else tag.Delete();
                }
                
                DelTagGenerator();

            };
            tagRemov.BorderThickness = new Thickness(0);
            tagRemov.Background = new SolidColorBrush()
            {
                Opacity = 0
            };
            tagRemov.Content = "x";
            tagRemov.FontSize = 8;
            Grid.SetColumn(tagRemov, 1);

            grid.Children.Add(tagBtn);
            grid.Children.Add(tagRemov);
            grid.Children.Add(border);

            return grid;
        }
        void DelTagGenerator()
        {
            int MaxLengTags = 60;
            for(int s = 0; s < stackPanels.Count -1; s++)
            {

               var lastTags = stackPanels[s].Children;
               int lengTags = 0;
                while (lengTags < MaxLengTags)
                {
                    //lengTags = 0;
                    for (int i = 0; i < lastTags.Count; i++)
                    {
                        lengTags += ((Button)((Grid)lastTags[i]).Children[0]).Content.ToString().Length + 5;
                    }
                    if (lengTags < MaxLengTags)
                    {
                        if(s+1 == stackPanels.Count-1 && stackPanels[s + 1].Children.Count == 1 && lengTags < MaxLengTags)
                        {
                            var NextChidren = stackPanels[s + 1].Children[0];
                            stackPanels[s + 1].Children.Remove(NextChidren);
                            stackPanels[s].Children.Add(NextChidren);
                            TagWindow.Children.Remove(stackPanels[s + 1]);
                            TagWindow.RowDefinitions.Remove(TagWindow.RowDefinitions[Grid.GetRow(stackPanels[s + 1])]);
                            stackPanels.Remove(stackPanels[s + 1]);
                            
                            break;

                        } 
                        else if (lengTags + (((Button)((Grid)stackPanels[s + 1].Children[0]).Children[0]).Content.ToString().Length + 5) <= MaxLengTags)
                        {
                            var NextChidren = stackPanels[s + 1].Children[0];
                            stackPanels[s + 1].Children.Remove(NextChidren);
                            stackPanels[s].Children.Add(NextChidren);
                        }
                    }
                }
               
            }

        }
        void TagRemod()
        {

        }
        void CreatNewStackPanels(UIElement tagBlock)
        {
            TagWindow.RowDefinitions.Add(new RowDefinition()
            {
                Height = new GridLength(40)
            });
            StackPanel tagsPanel = new StackPanel();
            tagsPanel.VerticalAlignment = VerticalAlignment.Top;
            tagsPanel.Orientation = Orientation.Horizontal;
            tagsPanel.Children.Add(tagBlock);
            tagsPanel.Margin = new Thickness(5);
            stackPanels.Add(tagsPanel);

            Grid.SetRow(tagsPanel, TagWindow.RowDefinitions.Count - 1);
            TagWindow.Children.Add(tagsPanel);
        }
                 

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            tagStudentTableAdapter.Update(tagStudents).ToString();
            tagsTableAdapter.Update(tags);
            
            for( int i =0; i < newaddtags.Count; i++)
            {
                if (newaddtags[i].RowState == System.Data.DataRowState.Deleted || newaddtagStudents[i].RowState == System.Data.DataRowState.Deleted) continue;
                newaddtagStudents[i].TagId = newaddtags[i].Id;
            }
            tagStudentTableAdapter.Update(newaddtagStudents).ToString();
            Close();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Window_Closed(object sender, EventArgs e)
        {
           
        }

        private void TagWindow_MouseUp(object sender, MouseButtonEventArgs e)
        {
            TagTBox.Focus();
        }
    }
}
