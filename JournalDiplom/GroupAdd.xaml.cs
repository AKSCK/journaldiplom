﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JournalDiplom
{
    /// <summary>
    /// Логика взаимодействия для GroupAdd.xaml
    /// </summary>
    public partial class GroupAdd : Window
    {
       
        public enum EditorState { Apply, Cancel };
        public EditorState CurretState { get; private set; } = EditorState.Cancel;
        public DataSet1.GroupsRow row = null;
        public DataSet1.GroupsRow Row
        {
            get
            {
                return row;
            }

            set
            {
                row = value;
                row.BeginEdit();

                this.DataContext = Row;
                if (!Row.IsNull("Base"))
                {
                    BaseCBox.SelectedIndex = row.Base == 9 ? 0 : 1;
                }
                else BaseCBox.SelectedIndex = 1;
            }

        }
        public GroupAdd()
        {
            InitializeComponent();
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string validEror = null;
            validEror += Utilities.CellValidate(Row, "Name", NameTBox.Text);
            if (!String.IsNullOrEmpty(validEror))
            {
                MessageBox.Show($"Не удалось внести изменения {validEror}");
                return;
            }
            row.Base = int.Parse(((TextBlock)BaseCBox.SelectedItem).Text);
            CurretState = EditorState.Apply;
            this.Close();


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CurretState = EditorState.Cancel;

            this.Close();

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (CurretState == EditorState.Apply) row.EndEdit();
            else row.CancelEdit();

        }
    }
}
