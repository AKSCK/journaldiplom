﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using JournalDiplom.DataSet1TableAdapters;

namespace JournalDiplom
{
    /// <summary>
    /// Логика взаимодействия для StudWindow.xaml
    /// </summary>
    public partial class StudWindow : Window
    {
        DataSet1.GroupStudentDataTable groupStudents = new DataSet1.GroupStudentDataTable();
        GroupStudentTableAdapter groupStudentTableAdapter = new GroupStudentTableAdapter();
        DataSet1.StudentsDataTable studentsDataTable = new DataSet1.StudentsDataTable();
        public  DataSet1.StudentsDataTable StudentsDataTable
        {
            get
            {
                return studentsDataTable;
            }
            set
            {
                studentsDataTable = value;
                StudentLView.ItemsSource = studentsDataTable.AsDataView();
            }
        }
        public int GroupId { get; set; }
        StudentsTableAdapter studentsTableAdapter = new StudentsTableAdapter();
        public StudWindow()
        {
            InitializeComponent();


        }

        private void remStudentBtn_Click(object sender, RoutedEventArgs e)
        {
            var row = ((DataRowView)StudentLView.SelectedValue)?.Row;
            if (row == null) return;
            row.Delete();
        }

        private void addStudentBtn_Click(object sender, RoutedEventArgs e)
        {
            var row = studentsDataTable.NewStudentsRow();
            StudEditorWindow studEditorWindow = new StudEditorWindow();
            studEditorWindow.StudentsRow = row;
            studEditorWindow.ShowDialog();
            if (studEditorWindow.CurretState == StudEditorWindow.EditorState.Apply)
            {
                try
                {
                    //если пользоватеь не сохранит будет лишняя строка groupStudents
                    
                    studentsDataTable.AddStudentsRow(row);
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Ошибка не удалось создать строку по причине {ex}");
                }
            }

        }

        private void editGroupBtn_Click(object sender, RoutedEventArgs e)
        {
            var row = ((DataRowView)StudentLView.SelectedValue)?.Row;
            if (row == null) return;
            StudEditorWindow studEditorWindow = new StudEditorWindow();
            studEditorWindow.StudentsRow = (DataSet1.StudentsRow)row;
            studEditorWindow.ShowDialog();

        }

        private void saveGroupBtn_Click(object sender, RoutedEventArgs e)
        {

            int count = 0;
            try
            {
              var studentsAdd = studentsDataTable.Where(s => s.RowState == DataRowState.Added).ToList();
                count = studentsTableAdapter.Update(studentsDataTable);

                
                foreach(var row in studentsAdd)
                {
                    DataSet1.GroupStudentRow groupStudentRow = groupStudents.NewGroupStudentRow();
                    groupStudentRow.StudentId = row.id;
                    groupStudentRow.GroupId = GroupId;
                    groupStudents.AddGroupStudentRow(groupStudentRow);
                }
                

                count += groupStudentTableAdapter.Update(groupStudents);

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка при сохранении {ex}");
            }

            if (count > 0)
            {
                MessageBox.Show("Сохранение выполнено успешно");
            }
        }
        
    }
}
