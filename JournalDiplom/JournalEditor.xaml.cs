﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JournalDiplom
{
    /// <summary>
    /// Логика взаимодействия для JournalAdd.xaml
    /// </summary>
    public partial class JournalEditor : Window
    {
        
        public enum EditorState { Apply, Cancel };
        public EditorState CurretState { get; private set; } = EditorState.Cancel;
        //public DataSet1.JournalRow row = null;
        public DataRowView rowView = null;

        public DataRowView RowView
        {
            get
            {
                return rowView;
            }

            set
            {
                rowView = value;
                rowView.BeginEdit();

                this.DataContext = rowView;
            }

        }

        //public DataSet1.JournalRow Row
        //{
        //    get
        //    {
        //        return row;
        //    }

        //    set
        //    {
        //        row = value;
        //        row.BeginEdit();

        //        this.DataContext = Row;
        //    }

        //}

        public JournalEditor()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string validEror = null;
            validEror += Utilities.CellValidate(rowView.Row, "MaxAllHourse", MaxAllHourse.Text);
            validEror += Utilities.CellValidate(rowView.Row, "MaxAllHourse", MaxHoursLectures.Text);
            validEror += Utilities.CellValidate(rowView.Row, "MaxAllHourse", MaxHoursPrak.Text);
            validEror += Utilities.CellValidate(rowView.Row, "MaxAllHourse", MaxHoursLab.Text);
            validEror += Utilities.CellValidate(rowView.Row, "MaxAllHourse", MaxHoursDop.Text);
            if (!String.IsNullOrEmpty(validEror))
            {
                MessageBox.Show($"Не удалось внести изменения {validEror}");
                return;
            }
            CurretState = EditorState.Apply;
            this.Close();


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CurretState = EditorState.Cancel;

            this.Close();

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (CurretState == EditorState.Apply)
            {
                //row.EndEdit();
                rowView.EndEdit();
            }
            else
            {
               // row.CancelEdit();
                rowView.CancelEdit();
            }

        }
    }
}
