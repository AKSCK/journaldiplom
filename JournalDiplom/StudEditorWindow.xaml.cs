﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using JournalDiplom.DataSet1TableAdapters;
namespace JournalDiplom
{
    /// <summary>
    /// Логика взаимодействия для StudEditorWindow.xaml
    /// </summary>
    public partial class StudEditorWindow : Window
    {
        public enum EditorState { Apply, Cancel };
        public EditorState CurretState { get; private set; } = EditorState.Cancel;
        DataSet1.StudentsRow studentsRow = null;
       
        public DataSet1.StudentsRow StudentsRow
        {
            get => studentsRow;
            set
            {
                studentsRow = value;
                studentsRow.BeginEdit();
                this.DataContext = studentsRow;

            }
        }
        public StudEditorWindow()
        {
            InitializeComponent();
        }

        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            string validEror = null;
            validEror += Utilities.CellValidate(StudentsRow, "Photo", PhotoEditor.Text);
            validEror += Utilities.CellValidate(StudentsRow, "Surname", SurnameEditor.Text);
            validEror += Utilities.CellValidate(StudentsRow, "Name", NameEditor.Text);
            validEror += Utilities.CellValidate(StudentsRow, "Patronymic", PatronymicEditor.Text);
            validEror += Utilities.CellValidate(StudentsRow, "Gender", GenderEditor.Text);
            if (!String.IsNullOrEmpty(validEror))
            {
                MessageBox.Show($"Не удалось внести изменения {validEror}");
                return;
            }


            CurretState = EditorState.Apply;
            this.Close();


        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            CurretState = EditorState.Cancel;

            this.Close();

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (CurretState == EditorState.Apply)
            {


                StudentsRow.EndEdit();

            }

            else
                StudentsRow.CancelEdit();

        }
    }
}

