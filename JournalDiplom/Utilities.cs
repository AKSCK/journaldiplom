﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JournalDiplom
{
    class Utilities
    {
        static Dictionary<string, string> UserReadlyNameOfTypes = new Dictionary<string, string>();
        public static string CellValidate(DataRow row, string column, string value)
        {
            //создаём переменую и записываем в неё название проверяемого столбца
            string columnAlterName = row.Table.Columns[column].Caption;
            //создаём переменую для хранения всех ошибок выявленых при валидации
            string validatorInfo = "";
            //Получаем тип значений поля 
            Type validType = row.Table.Columns[column].DataType;
            //создаём переменую для проверки, что введёные значения прошли валидацию 
            bool valueTypeIsValid = true;
            //Проверяем значение введёное пользователем на пустоту и на то может ли поле быть пустым
            if (String.IsNullOrEmpty(value) && !row.Table.Columns[column].AllowDBNull)
            {
                //Добавляем строку с ошибокой в переменую validatorInfo
                validatorInfo += $"Поле <{columnAlterName}> не может быть пустым\n";
            }
            try
            {
                //Попытка преобразовать введёное значение в тип поля
                Convert.ChangeType(value, validType);
            }
            catch (OverflowException exc)
            {
                //Если произойдёт ошибка OverflowException(слишком большое значение) помечаем, что значение не прошло проверку на валидность  
                valueTypeIsValid = false;
                //Добавляем строку с ошибокой в переменую validatorInfo
                validatorInfo += $"Попытка записи в <{columnAlterName}> слишком большое значение";
            }
            catch (Exception exc)
            {
                //В случае всех остольных ошибок помечаем, что значение не прошло проверку на валидность  
                valueTypeIsValid = false;
                // Преобразуем тип поля в понятный пользователю
                var typeNameForUser = (UserReadlyNameOfTypes.ContainsKey(validType.Name)) ? UserReadlyNameOfTypes[validType.Name] : validType.Name;
                //Добавляем строку с ошибокой в переменую validatorInfo
                validatorInfo += $"Поле <{columnAlterName}> должноиметь {typeNameForUser} тип\n";
            }
            //Если значение прошло проверку на валидность
            if (valueTypeIsValid)
            {
                //Проверяем должно ли значение поля быть уникальным
                if (row.Table.Columns[column].Unique)
                {
                    //Записываем в переменую rows список всех совпадающих строк с вводимым пользователем значением
                    var rows = row.Table
                        .AsEnumerable()
                        .Where(r =>
                        {
                            //Добавляем в список только те строки которые не помечены как удаленые и их поля совпадают со значением ввидёным пользователем 
                            return r.RowState != DataRowState.Deleted && r[column].Equals(row[column]);
                        })
                        .ToList();
                    //Проверяем что полученый список имеет только 1 строку, что этота строка не является той, что передали в этот метод, или если полученый список имеет больше 1 элемента в списке 
                    if (rows.Count == 1 && rows[0] != row || rows.Count > 1)
                        //Добавляем строку с ошибокой в переменую validatorInfo
                        validatorInfo += $"Запись с <{columnAlterName}> равным <{value}> уже есть в таблице\n";
                }
            }
            return validatorInfo;
        }
    }
}
